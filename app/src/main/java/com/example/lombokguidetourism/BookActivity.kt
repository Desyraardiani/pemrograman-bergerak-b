package com.example.lombokguidetourism

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import com.example.lombokguidetourism.ui.Booking.Booking

class BookActivity : AppCompatActivity() {
    private var desList: ArrayList<Destinasi> = arrayListOf()


    companion object {
//        var EXTRA_LIST = "extra_list"
        var EXTRA_POSITION = "extra_position"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_book)

//        val listDestinasi = intent.getStringExtra(EXTRA_LIST)
        val position = intent.getIntExtra(EXTRA_POSITION, 0)

        val tvItemName : TextView = findViewById(R.id.tv_nama_destinasi_book)
        val tvItemPhoto : ImageView = findViewById(R.id.tv_img_destinasi_book)
        val tvItemDetail : TextView = findViewById(R.id.tv_detail_destinasi_book)
        val tvItemDeskripsi : TextView = findViewById(R.id.tv_deskripsi_book)
        val btnPesan : AppCompatButton = findViewById(R.id.btn_pesan)

        desList.addAll(DesData.listData)
        tvItemName.setText(desList[position].nama)
        tvItemPhoto.setImageResource(desList[position].photo)
        tvItemDetail.setText(desList[position].lokasi)
        tvItemDeskripsi.setText(desList[position].deskripsi)

        btnPesan.setOnClickListener {
            val context = it.context
            val desDetailIntent = Intent(context, Booking::class.java)
            desDetailIntent.putExtra(Booking.EXTRA_POSITION, position)
            context.startActivity(desDetailIntent)
        }


/*
        val btnTel : Button = findViewById(R.id.btn_tlp)

        btnTel.setOnClickListener(this)

        when(listDestinasi){
            "Cat" -> {
                desList.addAll(CatData.listData)

                tvItemName.setText(desList[position].name)
                tvItemPhoto.setImageResource(desList[position].photo)
                tvItemDetail.setText(desList[position].detail)
            }
            "Dog" -> {
                dogList.addAll(DogData.listData)

                tvItemName.setText(dogList[position].name)
                tvItemPhoto.setImageResource(dogList[position].photo)
                tvItemDetail.setText(dogList[position].detail)
            }
            "Fish" -> {
                fishList.addAll(FishData.listData)

                tvItemName.setText(fishList[position].name)
                tvItemPhoto.setImageResource(fishList[position].photo)
                tvItemDetail.setText(fishList[position].detail)
            }
        }

*/

    }

/*    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_tlp -> {
                val phoneNumber = "087704620821"
                val dialPhoneIntent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:$phoneNumber"))
                startActivity(dialPhoneIntent)
            }
        }
    }*/
}