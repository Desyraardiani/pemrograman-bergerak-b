package com.example.lombokguidetourism

import com.example.lombokguidetourism.R

object DesData {
    private val DesNames = arrayOf(
        "Islamic Center",
        "Kota Tua Ampenan",
        "Pantai Ampenan",
        "Taman Air Mayura",
        "Taman Udayana",

       /* "Gili Kedis",
        "Pantai Mekaki",
        "Pantai Panggang",
        "Pantai Sengigi",
        "Sesaot",

        "Air Terjun Benang Kelambu",
        "Pantai Kuta",
        "Pantai Mawun",
        "Pantai Selong Belanak",
        "Pantai Tanjung Aan",

        "Air Terjun Jeruk Manis",
        "Air Terjun Mangku Sakti",
        "Gili Kondo",
        "Gunung Rinjani",
        "Pantai Tangsi",

        "Air Terjun Gangga",
        "Air Terjun Tiu Kelep",
        "Gili Meno",
        "Lombok Wildlife Park",
        "Pandanan"*/

    )
    private val DesDeskripsi = arrayOf(
        "Islamic Center Nusa Tenggara Barat menjadi salah satu destinasi wisata religi di Kota Mataram, Nusa Tenggara Barat. " +
                "Di dalam Islamic Center ini terdapat Masjid Raya Hubbul Wathan dan empat menara, di antaranya Menara Asmaul Husna dengan tinggi 99 meter. " +
                "Tidak hanya wisatawan muslim yang boleh mendatangi Masjid Raya Hubbul Wathan di lantai dua. Nonmuslim dibolehkan masuk tapi " +
                "diwajibkan mengenakan pakaian menutup aurat semacam jubah. Nantinya di lantai tiga akan disiapkan tourism zone (ruang area wisata), " +
                "yang dilengkapi berbagai informasi.",
        "Pantai Ampenan terkenal lantaran ada bangunan peninggalan sejarah " +
                "zaman penjajahan Belanda, Pelabuhan Ampenan dan Kota Tua Ampenan. " +
                "Saat ini Pantai Ampenan dijadikan salah satu destinasi wisata favorit di Kota Mataram. " +
                "Setiap akhir pekan hari-hari libur, Pantai ini selalu ramai dikunjungi para wisatawan, " +
                "khususnya domestik.",
        "Letak kota tua ini persis di dekat pantai Ampean. Banyak gedung atau bangunan tua dengan gaya art deco masih berdiri di berbagai " +
                "sudut kota. Bangunan yang terkesan kuno ini masih dipertahankan oleh warga setempat agar menjadi ikon yang menarik perhatian wisatawan. " +
                "Selain untuk menjadi destinasi wisata, kota tua ini juga kaya akan nilai sejarah. Pusat perekonomian pasa masa kolonial terjadi di kota " +
                "yang dekat dengan pantai ini. Oleh karena itu, banyak bangunan yang didirikan oleh pihak kolonial dan sampai sekarang masih bertahan.",
        "Taman Mayura merupakan taman yang dibangun pada masa Kerajaan Bali masih berkuasa di daerah Lombok pada tahun " +
                "1744 Masehi oleh Raja A.A. Made Karangasem. Wisatawan yang berkunjung ke objek wisata ini akan merasakan nuansa yang berbeda " +
                "yaitu perpaduan antara pemandangan alam yang asri, nuansa religius yang kental dan sejarah yang melekat di dalamnya. " +
                "Hal tersebut tentunya dikarenakan wilayah taman yang terbagi menjadi dua yaitu area taman dan area pura sebagai tempat ibadah.",
        "Aman Udayana adalah taman yang menjadi kebanggaan Kota Mataram. Taman ini berada di jalan " +
                "Udayana dan tepatnya berada di tengah-tengah pusatnya kota Mataram. Taman ini memiliki fungsi yang " +
                "tidak sedikit didalan dinamika kehidupan pada masyarakat kota Mataram ataupun luar Mataram.",

       /* "Pulau ini luasnya hanya beberapa ratus meter pesegi, kamu mungkin bisa mengitarinya dengan jalan kaki hanya dalam waktu 10 menit." +
                "Gili Kedis sepenuhnya terbentuk dari pasir putih yang halus." +
                "Cuma ada beberapa pohon dan bebatuan di sana. Tempat ini sempurna bagi kamu yang menyukai fotografi, terutama yang menggunakan drone." +
                "Lantaran bentuk pulau ini menyerupai hati, banyak juga yang memberinya label pulau cinta.",
        "Kalau kamu datang ke Pantai Mekaki, yang akan kamu dapat bukan cuma pantai yang indah, tetapi juga perjalanan yang berkesan." +
                "Sebelumnya pantai ini banyak disebut sebagai surga tersembunyi karena jarang dikunjungi wisatawan akibat aksesnya yang sulit." +
                "Tapi kini jalan menuju Mekaki sudah halus mulus." +
                "Jalanannya berkelok cantik dengan pemandangan perbukitan dan pantai yang aduhai. Ga akan sempat bosan deh selama di jalan.",
        "Pantai Panggang Buwun Mas memiliki air laut yang jernih dan pasir putih yang berbutiran halus." +
                "Ombak yang terbilang cukup besar di pagi hari semakin menghidupkan suasana pantai yang elok rupawan." +
                "Sedikit mirip dengan ujung selatan Bali di Tanjung Benoa, mulai Uluwatu hingga Gunung Payung." +
                "Hanya daerah yang berteluk saja yang bisa memiliki pantai dengan ombak tenang dan berpasir putih.",
        "Bisa dikatakan kalau Pantai Senggigi adalah destinasi pertama di pulau Lombok yang dikenal oleh dunia internasional." +
                "Garis pantai panjang di pesisir barat pulau dihiasi dengan deretan pantai-pantai indah berpasir putih." +
                "Jadi jangan kaget kalau kamu berjalan menyusuri pantai dan melihat seolah keindahan pantai ini seperti tak ada habisnya.",
        "Sesaot adalah hutan wisata yang masih alami, asri dan indah. Hutan ini banyak dikunjungi pada saat liburan sekolah." +
                "Biasanya dipakai untuk kegiatan outbond untuk mengisi mas liburan." +
                "Tidak jauh dari sesaot (masih di areal hutan wisata) terdapa sebuah sungai yang di sebut Aiq Nyet. " +
                "Sungai ini tidak pernah kering walaupun di musim kemarau." +
                "Keaslian alam yang ada memiliki daya tarik tersendiri bagi pengunjung. " +
                "Tidak hanya keindahan alam, akan tetapi juga di Sesaot pengunjung dapat menikmati hidangan masyarakat Lombok asli.",

        "Namanya unik karena aliran air ini tidak besar yang langsung mengarah ke kolam seperti pada umumnya. Aliran airnya terletak di beberapa titik yang" +
                "menyentuh pepohonan gambung yang rindang dan bebatuan sebelum mengalir ke kolam." +
                "Hasilnya, air terjun ini terlihat seperti kelambu atau tirai yang terpasang di tebing bebatuan.",
        "Pantai Kuta banyak dikunjungi wisatawan lokal maupun mancanegara. Kelebihannya adalah letak pantai yang dekat dengan jalan raya dan hotel-hotel tempat menginap." +
                "Selain itu, banyak masyarakat sekitar yang menjajakan makanan, minuman dan kain tradisional untuk oleh-oleh.",
        "Pantai Mawun menjadi salah satu pantai yang  wajib untuk dikunjungi apabila Anda sedang berada di Lombok Tengah. Pantai ini memiliki karateristik yang" +
                "cocok untuk Anda yang ingin menikmati keindahan dalam ketenangan karena wisata ini tidak terlalu ramai dikunjungi wisatawan." +
                "Pasir putih bersih terhampar luas di sepanjang pantai, air laut biru yang tenang dengan angin sepoi-sepoi memanjakan tubuh Anda. Di sudut pantai" +
                "terdapat bukit yang menambah keindahannya.",
        "Pantai yang terkenal indah di Lombok Tengah menjadi salah satu tempat wisata yang harus Anda kunjungi. Pantai ini diapit oleh dua bukit besar." +
                "Laut biru disana mempunyai gelombang yang cukup besar dan bisa dimanfaatkan untuk berselancar. Di lain sisi, ombak begitu tenang sehingga bisa berenang atau" +
                "sekedar bermain pasir.",
        "Pantai ini mempunyai keunikan tersendiri dibandingkan pantai-pantai lain yang ada di Lombok Tengah. Pantai ini memiliki dua karakteristik pasir yang berbeda." +
                "Di satu tempat, pasir putih bersih menghampar sepanjang garis pantai. Di tempat lain, pasir berwarna sedikit kecoklatan. Pantai Tanjung Aan ini merupakan pantai terpanjang" +
                "di Lombok Tengah. Masyarakat sekitar kemudian membagi pantai ini menjadi tiga bagian yaitu pantai Batu Kotak, pantai Orong dan pantai Cemara.",

        "Air terjun ini berada di wilayah Taman Nasional Gunung Rinjani. Artinya, lokasinya dilindungi dan gerbang masuknya pun melalui kantor taman nasional. Meski lokasinya cukup jauh, tempat ini" +
                "cukup mudah dicapai dengan kendaraan pribadi. Kamu bisa memarkir kendaraan kamu di kantor taman nasional dan dilanjutkan dengan berjalan kaki menyusuri hutan untuk sampai ke lokasi air terjun.",
        "Di Lombok Timur, kamu bisa menemukan air terjun Mangku Sakti. Konon, air terjun ini merupakan aliran air dari danau di kawah Rinjani, Danau Segara Anak." +
                "Mangku Sakti didominasi oleh banyak batuan dengan aliran air terjun yang deras. Tempat ini bisa terlihat luar biasa, baik dari dekat maupun dari jauh. Kolam di bawah air terjunnya berwarna hijau toska.",
        "Gili Kondo adalah satu dari gugusan pulau kecil yang berada di Selat Alas. Artinya, pulau-pulau itu berada di antara Lombok dan Sumbawa. Seperti kebanyakan pulau kecil di sekitar Lombok, " +
                "Gili Kondo juga sangat eksotis. Pulau ini sepenuhnya terdiri dari pasir putih yang bisa tenggelam kalau laut sedang pasang. Di sekitar Gili Kondo, ada beberapa gili lain yang juga sangat eksotis. " +
                "Ada Gili Pasir, Gili Pasaran, dan juga Gili Lampu. Kamu bisa mengeksplorasi semua pulau itu dengan mengendarai perahu jukung.",
        "Gunung Rinjani menjadi salah satu tujuan pendakian bagi para penjelajah gunung di Indonesia bahkan manca Negara. Dengan medan yang menantang dan bervariasi, Gunung Rinjani" +
                "menjadi salah satu gunung yang paling diburu para pendaki. Gunung Rinjani memiliki kawah dengan lebar sekitar 10 km dan terdapat danau kawah yang disebut danau Segara Anak dengan kedalaman " +
                "sekitar 230 m. Dengan warna airnya yang membiru bagaikan anak lautan, air yang mengalir dari danau ini membentuk air terjun yang sangat indah dan mengalir melewati jurang yang curam. ",
        "Tak banyak pantai di dunia ini yang memiliki pasir berwarna merah muda alias pink, tapi Indonesia punya beberapa, salah satunya ada di Lombok." +
                "Pantai Tangsi yang berada di Jerowaru memiliki warna alami merah muda yang memukau. Warna ini bisa tercipta karena perpaduan antara pasir putih dengan" +
                "serpihan karang merah yang sudah mati. Di sini, kamu bisa melihat gradasi warna yang tiada duanya. Dimulai dari pasir putih, merah muda, menuju air laut toska, " +
                "hingga biru gelap di kedalaman.",

        "Campuran warna hijau dan biru yang ada di air terjun ini menjadi sesuatu yang berbeda yang sangat menarik bagi para pengunjung. Air terjun ini terletak di hamparan" +
                "perbukitan di mana di sekitarnya terdapat pula daerah persawahan dan lautan lepas.",
        "Air Terjun Tiu Kelep merupakan air terjun yang berada di kaki Gunung Rinjani. Lokasinya berdekatan dengan Air Terjun Sendang Gile. Air terjun tersebut terkenal karena keindahannya dan memiliki" +
                "tinggi mencapai 42 meter. Dalam bahasa Sasak, air terjun tersebut memiliki arti Kolam Renang \"Tiu\" dan \"Kelep\" berarti Terbang. Jika digabungkan, bisa berarti Kolam Renang Terbang.",
        "Letak Gili Meno tidak jauh dari Gili Trawangan. Pulau kecil ini juga menyimpan keindahan alam yang begitu luar biasa. Di Gili Meno, Anda bisa menikmati keindahan pantai yang sangat eksotis." +
                "Pantai di Gili Meno begitu cantik dengan lautannya yang membiru dan tenang.",
        "Lombok Wild Life Park yang merupakan kebun binatang pertama di Lombok tentu menarik banyak wisatawan. Selain dapat menikmati" +
                "pesona alam yang masih alami, Teman Traveler juga dapat berinteraksi langsung dengan berbagai macam koleksi heran endemik Indonesia seperti" +
                "burung kakak tua, orang utan dan masih banyak lagi yang lainnya.",
        "Pandanan adalah objek wisata pantai yang ada di Lombok Utara. Letak pantai ini tidak terlalu jauh dari Pantai Senggigi yang juga menjadi ikon wisata setempat." +
                "Nah, Pantai Pandanan cenderung tidak terlalu ramai oleh pengunjung karena memang belum di eksplorasi secara mendalam." +
                "Oleh karena itu, bagi Anda yang ingin berkunjung ke pantai yang lebih private, maka Pantai Pandanan bisa menjadi pilihan."
    */)

    private val DesLokasi = arrayOf(
        "Gomong, Kota Mataram",
        "Ampenan, Kota Mataram",
        "Ampenan, Kota Mataram",
        "Mayura, Kota Mataram",
        "Selaparang, Kota Mataram",

        /*"Sekotong Tenggah, Lombok Barat",
        "Pelangan, Lombok Barat",
        "Sekotong Tengah, Lombok Barat",
        "Batu Layar, Lombok Barat",
        "Narmada, Lombok Barat",

        "Batukliang Utara, Lombok Tengah",
        "Kuta, Lombok Tengah",
        "Pujut, Lombok Tenggah",
        "Praya Barat, Lombok Tengah",
        "Kuta, Lombok Tengah",

        "Sikur, Lombok Timur",
        "Sajang, Lombok Timur",
        "Sambelia, Lombok Timur",
        "Sembalun lawang, Lombok Timur",
        "Jerowaru, Lombok Timur",

        "Gangga, Lombok Utara",
        "Bayan, Lombok Utara",
        "Pemenang, Lombok Utara",
        "Tanjung, Lombok Utara",
        "Pemenang, Lombok Utara"*/

    )

    private val DesImages = intArrayOf(
            R.drawable.mataram_islamic_center,
            R.drawable.mataram_kota_tua,
            R.drawable.mataram_pantai_ampenan,
            R.drawable.mataram_taman_air_mayura,
            R.drawable.mataram_taman_udayana,

           /* R.drawable.lobar_gili_kedis,
            R.drawable.lobar_pantai_mekaki,
            R.drawable.lobar_pantai_panggang,
            R.drawable.lobar_pantai_sengigi,
            R.drawable.lobar_sesaot,

            R.drawable.loten_air_terjun_benang_kelambu,
            R.drawable.loteng_pantai_kuta,
            R.drawable.loteng_pantai_mawun,
            R.drawable.loteng_pantai_selong_belanak,
            R.drawable.loteng_pantai_tanjung_aan,

            R.drawable.lotim_air_terjun_jeruk_manis,
            R.drawable.lotim_air_terjun_mangku_sakti,
            R.drawable.lotim_gili_kondo,
            R.drawable.lotim_gunung_rinjani,
            R.drawable.lotim_pantai_tangsi,

            R.drawable.lombok_utara_air_terjun_gangga,
            R.drawable.lombok_utara_air_terjun_tiu_kelep,
            R.drawable.lombok_utara_gili_meno,
            R.drawable.lombok_utara_lombok_wildlife_park,
            R.drawable.lombok_utara_pandanan*/
    )



    val listData: ArrayList<Destinasi>

        get() {
            val list = arrayListOf<Destinasi>()
            for (position in DesNames.indices) {
                val des = Destinasi()
                des.nama = DesNames[position]
                des.lokasi = DesLokasi[position]
                des.photo = DesImages[position]
                des.deskripsi = DesDeskripsi[position]

                list.add(des)
            }
            return list
        }
}
