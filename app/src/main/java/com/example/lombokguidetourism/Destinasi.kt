package com.example.lombokguidetourism

data class Destinasi (
    var nama : String = "",
    var lokasi : String = "",
    var photo : Int = 0,
    var deskripsi : String = ""
)