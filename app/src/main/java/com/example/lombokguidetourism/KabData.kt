package com.example.lombokguidetourism

object KabData {
    private val kabNames = arrayOf(
        "Mataram",
        "Lombok Barat",
        "Lombok Tengah",
        "Lombok Timur",
        "Lombok Utara",
    )

    private val label = arrayOf(
        "Kota",
        "Kabupaten",
        "Kabupaten",
        "Kabupaten",
        "Kabupaten",
    )

    private val kabImages = intArrayOf(
            R.drawable.mataram,
            R.drawable.lobar,
            R.drawable.loteng,
            R.drawable.lotim,
            R.drawable.lotur,
    )

    val listData: ArrayList<Kabupaten>
        get() {
            val list = arrayListOf<Kabupaten>()
            for (position in kabNames.indices) {
                val kab = Kabupaten()
                kab.name = kabNames[position]
                kab.label = label[position]
                kab.photo = kabImages[position]

                list.add(kab)
            }
            return list
        }
}
