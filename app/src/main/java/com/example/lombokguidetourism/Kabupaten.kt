package com.example.lombokguidetourism

data class Kabupaten(
    var name: String = "",
    var label: String = "",
    var photo: Int = 0
)
