package com.example.lombokguidetourism

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class ListDesAdapter(private val listDestinasi: ArrayList<Destinasi>) : RecyclerView.Adapter<ListDesAdapter.ListViewHolder>() {

    inner class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvName: TextView = itemView.findViewById(R.id.tv_nama_destinasi)
        var tvLabel: TextView = itemView.findViewById(R.id.tv_detail_destinasi)
        var imgPhoto: ImageView = itemView.findViewById(R.id.tv_img_destinasi)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ListViewHolder {
        val view: View = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_list_destinasi, viewGroup, false)
        return ListViewHolder(view)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        val Destinasi = listDestinasi[position]

        Glide.with(holder.itemView.context)
            .load(Destinasi.photo)
            .apply(RequestOptions().override(55, 55))
            .into(holder.imgPhoto)
        holder.tvName.text = Destinasi.nama
        holder.tvLabel.text = Destinasi.lokasi

//        Buat kalo di klik
        holder.tvName.setOnClickListener {
            val context = it.context
            val desDetailIntent = Intent(context, BookActivity::class.java)
            desDetailIntent.putExtra(BookActivity.EXTRA_POSITION, position)
            context.startActivity(desDetailIntent)
        }

        holder.tvLabel.setOnClickListener {
            val context = it.context
            val desDetailIntent = Intent(context, BookActivity::class.java)
            desDetailIntent.putExtra(BookActivity.EXTRA_POSITION, position)
            context.startActivity(desDetailIntent)
        }

        holder.imgPhoto.setOnClickListener {
            val context = it.context
            val desDetailIntent = Intent(context, BookActivity::class.java)
            desDetailIntent.putExtra(BookActivity.EXTRA_POSITION, position)
            context.startActivity(desDetailIntent)
        }
    }



    override fun getItemCount(): Int {
        return listDestinasi.size
    }

}