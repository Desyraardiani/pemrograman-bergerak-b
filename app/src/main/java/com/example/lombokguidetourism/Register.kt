package com.example.lombokguidetourism

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.AppCompatButton
import com.example.lombokguidetourism.signup.Signup
import com.example.lombokguidetourism.ui.login.LoginActivity

class Register : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        val btnRegister: AppCompatButton = findViewById(R.id.gotoSignup)
        btnRegister.setOnClickListener(this)
        val btnLogin: AppCompatButton = findViewById(R.id.gotoLogin)
        btnLogin.setOnClickListener(this)
    }

    override fun onClick(v: View?){
        when (v?.id){
            R.id.gotoLogin ->{
//                val login = Intent(this, LoginActivity::class.java)
//                startActivity(login)
                val login = Intent(this, LoginActivity::class.java)
                startActivity(login)
            }
            R.id.gotoSignup ->{
//                val gotobooking = Intent(this, Booking::class.java)
//                startActivity(gotobooking)
                val signup = Intent(this, Signup::class.java)
                startActivity(signup)
            }
        }
    }
}