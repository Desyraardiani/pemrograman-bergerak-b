package com.example.lombokguidetourism

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.example.lombokguidetourism.ui.nav_bar.BottomNavigationBar
import com.google.firebase.auth.FirebaseAuth

@Suppress("DEPRECATION")
class SplashScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)


        // This is used to hide the status bar and make
        // the splash screen as a full screen activity.
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )


        // we used the postDelayed(Runnable, time) method
        // to send a message with a delayed time.
        Handler().postDelayed({
            if (FirebaseAuth.getInstance().currentUser != null)
            {
                val intent = Intent(this@SplashScreen, BottomNavigationBar::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                finish()
            }else{
            val intent = Intent(this, Register::class.java)
            startActivity(intent)
            finish()}
        }, 3000) // 3000 is the delayed time in milliseconds.
    }
}
