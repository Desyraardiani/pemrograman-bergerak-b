package com.example.lombokguidetourism.signup

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import com.example.lombokguidetourism.R
import com.example.lombokguidetourism.ui.login.LoginActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

import kotlinx.android.synthetic.main.activity_signup.*
import kotlinx.android.synthetic.main.activity_login.*
import java.util.*
import kotlin.collections.HashMap

class Signup : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)


        btn_Signup.setOnClickListener {
            CreateAccount()
        }
    }

    private fun CreateAccount()
    {
        val fullName = fullName.text.toString()
        val Alamat = alamat.text.toString()
        val email = emailSignup.text.toString()
        val password = passwordSignup.text.toString()

        when{
            TextUtils.isEmpty(fullName) -> Toast.makeText(this, "full name is required.", Toast.LENGTH_LONG).show()
            TextUtils.isEmpty(Alamat) -> Toast.makeText(this, "user name is required.", Toast.LENGTH_LONG).show()
            TextUtils.isEmpty(email) -> Toast.makeText(this, "email is required.", Toast.LENGTH_LONG).show()
            TextUtils.isEmpty(password) -> Toast.makeText(this, "password is required.", Toast.LENGTH_LONG).show()

            else -> {
                val progressDialog = ProgressDialog(this@Signup)
                progressDialog.setTitle("SignUp")
                progressDialog.setMessage("Please wait, this may take a while...")
                progressDialog.setCanceledOnTouchOutside(false)
                progressDialog.show()

                val mAuth: FirebaseAuth = FirebaseAuth.getInstance()
                mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener{ task ->
                        if (task.isSuccessful)
                        {
                            saveUserInfo(fullName, Alamat, email, progressDialog)
                        }
                        else
                        {
                            val message = task.exception!!.toString()
                            Toast.makeText(this, "Error: $message", Toast.LENGTH_LONG).show()
                            mAuth.signOut()
                            progressDialog.dismiss()
                        }
                    }
            }
        }
    }



    private fun saveUserInfo(fullName: String, Alamat: String, email: String, progressDialog: ProgressDialog)
    {
        val currentUserID = FirebaseAuth.getInstance().currentUser!!.uid
        val usersRef: DatabaseReference = FirebaseDatabase.getInstance().reference.child("Users")

        val userMap = HashMap<String, Any>()
        userMap["uid"] = currentUserID
        userMap["fullname"] = fullName.toLowerCase()
        userMap["Alamat"] = Alamat
        userMap["email"] = email
        userMap["image"] = "https://firebasestorage.googleapis.com/v0/b/instagram-clone-app-ec78a.appspot.com/o/Default%20Images%2Fprofile.png?alt=media&token=aac2c4a5-5e20-4390-bded-ef05d53b9348"

        usersRef.child(currentUserID).setValue(userMap)
            .addOnCompleteListener { task ->
                if (task.isSuccessful)
                {
                    progressDialog.dismiss()
                    Toast.makeText(this, "Account has been created successfully.", Toast.LENGTH_LONG).show()
                    val intent = Intent(this@Signup, LoginActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                }
                else
                {
                    val message = task.exception!!.toString()
                    Toast.makeText(this, "Error: $message", Toast.LENGTH_LONG).show()
                    FirebaseAuth.getInstance().signOut()
                    progressDialog.dismiss()
                }
            }
        }
    }



/*
class Signup : AppCompatActivity() {

    private var etFirstName: EditText? = null
    private var etAlamat: EditText? = null
    private var etEmail: EditText? = null
    private var etPassword: EditText? = null
    private var etDateNaissance: EditText? = null
    private var btnCreateAccount: Button? = null
    private var mProgressBar: ProgressDialog? = null

    //Firebase references
    private var mDatabaseReference: DatabaseReference? = null
    private var mDatabase: FirebaseDatabase? = null
    private var mAuth: FirebaseDatabase? = null

    private val TAG = "Signup"

    //Global variables
    private var firstName: String? = null
    private var lastName: String? = null
    private var email: String? = null
    private var password: String? = null
    private var dateNaissance: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

//Initialise the references of Firebase database
        initialise()
    }

    //Function who initalise this references
    private fun initialise() {
        etFirstName = findViewById(R.id.fullName) as EditText
        etAlamat = findViewById(R.id.alamat) as EditText
        etEmail = findViewById(R.id.emailSignup) as EditText
        etPassword = findViewById(R.id.passwordSignup) as EditText
        //etDateNaissance = findViewById(R.id.signUp_dateNaissance) as EditText
        btnCreateAccount = findViewById(R.id.btn_Signup) as Button
        mProgressBar = ProgressDialog(this)

        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference?.child("Users")
        mDatabaseReference = mDatabase?.reference?.child("Users")

        btnCreateAccount!!.setOnClickListener {
            createNewAccount()
        }
    }

    private fun createNewAccount() {

        val mAuth = FirebaseAuth.getInstance()

//We get current string values present in EditText gaps
        firstName = etFirstName?.text.toString()
        lastName = etLastName?.text.toString()
        email = etEmail?.text.toString()
        password = etPassword?.text.toString()
        //dateNaissance = etDateNaissance?.text.toString()

//if there is text in the gaps
        if (!TextUtils.isEmpty(firstName) && !TextUtils.isEmpty(lastName)
            && !TextUtils.isEmpty(email) && !TextUtils.isEmpty(password) //&&
            //TextUtils.isEmpty(dateNaissance)
        ) {
//we create a new user
            val mAuth = FirebaseAuth.getInstance()
            mAuth
                .createUserWithEmailAndPassword(email!!, password!!)
                .addOnCompleteListener(this) { task ->
                    mProgressBar!!.hide()

                    if (task.isSuccessful) {
//Sign in success, Update Ui with the signed-in user’s information
                        Log.d(TAG, "createUserWithEmail : success ")
                        val userId = mAuth!!.currentUser!!.uid

//Verify Email
                        verifyEmail()

//Update user profile information
                        val currentUserDb = mDatabaseReference!!.child(userId)
                        currentUserDb.child("fisrtname").setValue(firstName)
                        currentUserDb.child("lastName").setValue(lastName)
                        currentUserDb.child("email").setValue(email)
                        currentUserDb.child("password").setValue(password)
                        //currentUserDb.child(“ dateNaissance ”).setValue(dateNaissance)

                        updateUserInfoAndUI()
                    } else {
//If sign in fails, display a message to the user
                        Log.w(TAG, "createUserWithEmail : failure ", task.exception)
                        Toast.makeText(this, " Authentication failed", Toast.LENGTH_SHORT).show()

                    }
                }

//We show the progress bar while the registration is done
            mProgressBar!!.setMessage("Please Wait... ")
            mProgressBar!!.show()

        } else {
            Toast.makeText(this, " User Registration Failed", Toast.LENGTH_SHORT).show()
        }
    }

    private fun updateUserInfoAndUI() {
        val intent = Intent(this, LoginActivity::class.java)
//The FLAG_ACTIVITY_CLEAR_TOP clear the precedent activity so that if
// user press back from the next activity, he should not be taken back
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
    }

    private fun verifyEmail() {
        val mAuth = FirebaseAuth.getInstance()
        val mUser = mAuth!!.currentUser
        mUser!!.sendEmailVerification()
            .addOnCompleteListener(this) { task ->

                if (task.isSuccessful) {
                    Toast.makeText(
                        this,
                        "Verification Email sent to "+mUser.email,
                    Toast.LENGTH_SHORT
                    ).show()
                } else {
                    Log.e(TAG, "sendEmailVerification ", task.exception)
                    Toast.makeText(this, " Failed to end verification email ", Toast.LENGTH_SHORT)
                    .show()
                }
            }
    }
}*/
