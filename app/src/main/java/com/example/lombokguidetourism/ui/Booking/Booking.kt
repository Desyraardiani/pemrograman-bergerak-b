package com.example.lombokguidetourism.ui.Booking

import android.app.DatePickerDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.DatePicker
import android.widget.TextView
import java.text.SimpleDateFormat
import android.view.View
import androidx.fragment.app.Fragment

import com.example.lombokguidetourism.BookActivity
import com.example.lombokguidetourism.DesData
import com.example.lombokguidetourism.Destinasi
import com.example.lombokguidetourism.R
import com.example.lombokguidetourism.ui.nav_bar.BottomNavigationBar
import com.example.lombokguidetourism.ui.nav_bar.TiketFragment
import kotlinx.android.synthetic.main.activity_booking.*
import java.util.*

class Booking : AppCompatActivity() {
    private var desList: ArrayList<Destinasi> = arrayListOf()
    companion object {
        //        var EXTRA_LIST = "extra_list"
        var EXTRA_POSITION = "extra_position"
    }
    var textview_date: TextView? = null
    var cal = Calendar.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booking)
        // get the references from layout file
        textview_date = findViewById(R.id.button_date_view)
        val position = intent.getIntExtra(BookActivity.EXTRA_POSITION, 0)
        val tvItemName : TextView = findViewById(R.id.editTujuan)
        desList.addAll(DesData.listData)
        tvItemName.setText(desList[position].nama)
        val tiketFragment = TiketFragment()

        textview_date!!.text = "Pilih Tanggal"

        // create an OnDateSetListener
        val dateSetListener = object : DatePickerDialog.OnDateSetListener {
            override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int,
                                   dayOfMonth: Int) {
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, monthOfYear)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateDateInView()
            }
        }

        // when you click on the button, show DatePickerDialog that is set with OnDateSetListener
        textview_date!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View) {
                DatePickerDialog(this@Booking,
                    dateSetListener,
                    // set DatePickerDialog to point to today's date when it loads up
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH)).show()
            }

        })

        save_booking.setOnClickListener {
            val context = it.context
            val desDetailIntent = Intent(context, BottomNavigationBar::class.java)

            desDetailIntent.putExtra(BottomNavigationBar.EXTRAA_POSITION, 1)
            context.startActivity(desDetailIntent)
        }

    }
    private fun updateDateInView() {
        val myFormat = "MM/dd/yyyy" // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        textview_date!!.text = sdf.format(cal.getTime())
    }

    private fun makeCurrentFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.container_fragment, fragment)
            commit()
        }
    }
}
