package com.example.lombokguidetourism.ui.nav_bar

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.lombokguidetourism.BookActivity
import com.example.lombokguidetourism.R
import com.example.lombokguidetourism.ui.Booking.Booking
import kotlinx.android.synthetic.main.activity_booking.*



class BottomNavigationBar : AppCompatActivity() {
    companion object {
        //        var EXTRA_LIST = "extra_list"
        var EXTRAA_POSITION = "extra_position"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bottom_navigation_bar)

        val position = intent.getIntExtra(EXTRAA_POSITION, 0)

        val homeFragment = HomeFragment()
        val tiketFragment = TiketFragment()
        val inboxFragment = InboxFragment()
        val profilFragment = ProfilFragment()

        val nav_bar : com.google.android.material.bottomnavigation.BottomNavigationView =  findViewById(R.id. bottom_nav_bar)

        if(position == 1){
            makeCurrentFragment(tiketFragment)
        }else{
            makeCurrentFragment(homeFragment)
        }




        nav_bar.setOnNavigationItemSelectedListener {
            when (it.itemId){
                R.id.home -> makeCurrentFragment(homeFragment)
                R.id.tiket -> makeCurrentFragment(tiketFragment)
                R.id.inbox -> makeCurrentFragment(inboxFragment)
                R.id.profil -> makeCurrentFragment(profilFragment)
            }
            true
        }

    }

    private fun makeCurrentFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.container_fragment, fragment)
            commit()
        }
    }
}
