package com.example.lombokguidetourism.ui.nav_bar

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.lombokguidetourism.*
import com.example.lombokguidetourism.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    //Firebase references
    private var mDatabaseReference: DatabaseReference? = null
    private var mDatabase: FirebaseDatabase? = null
    private var mAuth: FirebaseAuth? = null
    private var tvEmail: TextView? = null


    //UI elements
    private var tvFullname: TextView? = null
    private var imageView: CircleImageView? = null

    private fun initialise() {
        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase!!.reference!!.child("Users")
        mAuth = FirebaseAuth.getInstance()
        tvFullname = getView()?.findViewById(R.id.nama) as TextView
        imageView = getView()?.findViewById(R.id.img_home) as CircleImageView

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }


    private lateinit var rvKab: RecyclerView
    private var listKab: ArrayList<Kabupaten> = arrayListOf()

    private lateinit var rvDes: RecyclerView
    private var listDes: ArrayList<Destinasi> = arrayListOf()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        initialise()
        val mUser = mAuth!!.currentUser
        val mUserReference = mDatabaseReference!!.child(mUser!!.uid)
        //tvEmail!!.text = mUser.email
        mUserReference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                tvFullname!!.text = snapshot.child("fullname").value as String
                val imgaeUrl = snapshot.child("image").value as String
                //Glide.with(this).load(imgaeUrl).into(imageView)
                Picasso.get().load(imgaeUrl).into(imageView)
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })


        rvKab = view.findViewById(R.id.rv_kabupaten)
        rvKab.setHasFixedSize(true)

        rvDes = view.findViewById(R.id.rv_destinasi)
        rvDes.setHasFixedSize(true)




        if(listKab.isEmpty()){
            listKab.addAll(KabData.listData)
        }

        if(listDes.isEmpty()){
            listDes.addAll(DesData.listData)
        }

        showRecyclerListKabupaten()
        showRecyclerListDestinasi()


    }

    private fun showRecyclerListKabupaten() {
        rvKab.layoutManager = LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false)
        val listKabAdapter = ListKabAdapter(listKab)
        rvKab.adapter = listKabAdapter
    }
    private fun showRecyclerListDestinasi() {
        rvDes.layoutManager = LinearLayoutManager(getContext())
        val listDesAdapter = ListDesAdapter(listDes)
        rvDes.adapter = listDesAdapter
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment HomeFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            HomeFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}