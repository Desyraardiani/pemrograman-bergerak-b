package com.example.lombokguidetourism.ui.nav_bar

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import com.bumptech.glide.Glide
import com.example.lombokguidetourism.R
import com.example.lombokguidetourism.ui.login.LoginActivity
import com.example.lombokguidetourism.ui.profil.ProfilPengaturan
import com.example.lombokguidetourism.ui.profil.UbahData
import com.example.lombokguidetourism.ui.profil.Model.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.fragment_profil.view.*
import kotlinx.android.synthetic.main.fragment_profil.*
import kotlin.Unit.toString

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ProfilFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ProfilFragment : Fragment() {

    //Firebase references
    private var mDatabaseReference: DatabaseReference? = null
    private var mDatabase: FirebaseDatabase? = null
    private var mAuth: FirebaseAuth? = null

    //UI elements
    private var tvFullname: TextView? = null
    private var tvAlamat: TextView? = null
    private var tvEmail: TextView? = null
    private var imageView: CircleImageView? = null


    /*private lateinit var profileId: String
    private lateinit var firebaseUser: FirebaseUser*/


/*    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }*/

    private fun initialise() {
        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase!!.reference!!.child("Users")
        mAuth = FirebaseAuth.getInstance()
        tvFullname = getView()?.findViewById(R.id.tv_nama) as TextView
        tvAlamat = getView()?.findViewById(R.id.tv_alamat) as TextView
        tvEmail = getView()?.findViewById(R.id.tv_email) as TextView
        imageView = getView()?.findViewById(R.id.img_photo1) as CircleImageView
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profil, container, false)
        /*firebaseUser = FirebaseAuth.getInstance().currentUser!!
        val pref = context?.getSharedPreferences("PREFS", Context.MODE_PRIVATE)
        if (pref != null)
        {
            this.profileId = pref.getString("profileId", "none")!!
        }
        userInfo()
        return view*/
    }

    /* private fun userInfo()
    {
        val usersRef = FirebaseDatabase.getInstance().getReference().child("Users").child(profileId)

        usersRef.addValueEventListener(object : ValueEventListener
        {
            override fun onDataChange(p0: DataSnapshot)
            {
                if (p0.exists())
                {
                    val user = p0.getValue<User>(User::class.java)

                    //Picasso.get().load(user!!.getImage()).into(view?.img_photo1)
                    view?.tv_nama?.text = user!!.getFullname()
                    view?.tv_alamat?.text = user!!.getAlamat()
                    view?.tv_email?.text = user!!.getEmail()
                }
            }

            override fun onCancelled(p0: DatabaseError) {

            }
        })
    }*/

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initialise()
        val mUser = mAuth!!.currentUser
        val mUserReference = mDatabaseReference!!.child(mUser!!.uid)
        tvEmail!!.text = mUser.email
        mUserReference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                tvFullname!!.text = snapshot.child("fullname").value as String
                tvAlamat!!.text = snapshot.child("Alamat").value as String
                tvEmail!!.text = snapshot.child("email").value as String
                val imgaeUrl = snapshot.child("image").value as String
                //Glide.with(this).load(imgaeUrl).into(imageView)
                Picasso.get().load(imgaeUrl).into(imageView)
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })

        btn_signout.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            activity?.let {
                val intent = Intent(it, LoginActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }

        }
        //Pindah dari fragment ke activity
        btn_profil_pengaturan.setOnClickListener {
            activity?.let {
                val inten = Intent(it, ProfilPengaturan::class.java)
                it.startActivity(inten)
            }
        }

        btn_profil_ubahData.setOnClickListener {
            activity?.let {
                val inten2 = Intent(it, UbahData::class.java)
                it.startActivity(inten2)
            }
        }

    }

    /* override fun onStop() {
        super.onStop()

        val pref = context?.getSharedPreferences("PREFS", Context.MODE_PRIVATE)?.edit()
        pref?.putString("profileId", firebaseUser.uid)
        pref?.apply()
    }

    override fun onPause() {
        super.onPause()

        val pref = context?.getSharedPreferences("PREFS", Context.MODE_PRIVATE)?.edit()
        pref?.putString("profileId", firebaseUser.uid)
        pref?.apply()
    }

    override fun onDestroy() {
        super.onDestroy()

        val pref = context?.getSharedPreferences("PREFS", Context.MODE_PRIVATE)?.edit()
        pref?.putString("profileId", firebaseUser.uid)
        pref?.apply()
    }*/

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ProfilFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ProfilFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
