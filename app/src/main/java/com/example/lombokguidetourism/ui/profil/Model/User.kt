package com.example.lombokguidetourism.ui.profil.Model


class User
{
    private var fullname: String = ""
    private var Alamat: String = ""
    private var image: String = ""
    private var email: String = ""
    private var uid: String = ""


    constructor(){}


    constructor(Alamat: String, fullname: String, image: String, uid: String, email: String)
    {
        this.Alamat = Alamat
        this.fullname = fullname
        this.image = image
        this.email = email
        this.uid = uid
    }

    fun getEmail():String
    {
        return email
    }

    fun setEmail(): String
    {
        return email
    }

    fun getAlamat(): String
    {
        return  Alamat
    }

    fun setAlamat(alamat: String)
    {
        this.Alamat = alamat
    }


    fun getFullname(): String
    {
        return  fullname
    }

    fun setFullname(fullname: String)
    {
        this.fullname = fullname
    }


    fun getImage(): String
    {
        return  image
    }

    fun setImage(image: String)
    {
        this.image = image
    }


    fun getUID(): String
    {
        return  uid
    }

    fun setUID(uid: String)
    {
        this.uid = uid
    }
}